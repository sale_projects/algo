<?php
/**
 * Plugin Name:     HD Locations VN
 * Plugin URI:      http://q-tek.vn/
 * Description:     Multiple loations
 * Author:          Hieu Nguyen
 * Author URI:      #
 * Text Domain:     hd-locations-vn
 * Version:         1.0.0
 *
 */

/**
 * Exit if accessed directly.
 */
if ( ! defined( 'ABSPATH' ) ) {
	exit();
}
if ( ! class_exists( 'WP_List_Table' ) ) {
	require_once( ABSPATH . 'wp-admin/includes/class-wp-list-table.php' );
}
require_once plugin_dir_path(__FILE__) . 'includes/hd-functions.php';
require_once plugin_dir_path(__FILE__) . 'includes/class-location-table.php';
require_once plugin_dir_path(__FILE__) . 'includes/class-hd-location.php';

