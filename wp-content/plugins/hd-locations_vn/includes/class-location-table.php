<?php 
class Locationvn_Table extends WP_List_Table {

	/** Class constructor */
	public function __construct() {
		parent::__construct( [
		'singular' => __( 'Location', 'hd-locations' ), //singular name of the listed records
		'plural' => __( 'Locations', 'hd-locations' ), //plural name of the listed records
		'ajax' => false //should this table support ajax?

		] );

	}
	/**
	* Retrieve location’s data from the database
	*
	* @param int $per_page
	* @param int $page_number
	*
	* @return mixed
	*/
	public static function get_locations( $per_page = 5, $page_number = 1 ) {

		global $wpdb;

		$sql = "SELECT ID,name,address,city,phone,email,opening,case when `ishome`=1 then 'Yes' else 'No' end ishome FROM {$wpdb->prefix}locationsvn";

		if ( ! empty( $_REQUEST['orderby'] ) ) {
		$sql .= ' ORDER BY ' . esc_sql( $_REQUEST['orderby'] );
		$sql .= ! empty( $_REQUEST['order'] ) ? ' ' . esc_sql( $_REQUEST['order'] ) : ' ASC';
		}

		$sql .= " LIMIT $per_page";

		$sql .= ' OFFSET ' . ( $page_number - 1 ) * $per_page;

		$result = $wpdb->get_results( $sql, 'ARRAY_A' );

		return $result;
	}
	/**
	* Delete a location record.
	*
	* @param int $id location ID
	*/
	public static function delete_location( $id ) {
		global $wpdb;

		$wpdb->delete(
		"{$wpdb->prefix}locationsvn",
		[ 'ID' => $id ],
		[ '%d' ]
		);
	}
	/**
	* Returns the count of records in the database.
	*
	* @return null|string
	*/
	public static function record_count() {
		global $wpdb;

		$sql = "SELECT COUNT(*) FROM {$wpdb->prefix}locationsvn";

		return $wpdb->get_var( $sql );
	}
	/** Text displayed when no location data is available */
	public function no_items() {
		_e( 'No location avaliable.', 'hd-locations' );
	}
	/**
	* Method for name column
	*
	* @param array $item an array of DB data
	*
	* @return string
	*/
	function column_name( $item ) {

		// create a nonce
		$delete_nonce = wp_create_nonce( 'sp_delete_location' );

		$title = '<strong>' . $item['name'] . '</strong>';

		$actions = [
		'edit'      => sprintf('<a href="?page=%s&action=%s&location=%s">Edit</a>',$_REQUEST['page'],'edit',$item['ID']),
		'delete' => sprintf( '<a href="?page=%s&action=%s&location=%s&_wpnonce=%s">Delete</a>', esc_attr( $_REQUEST['page'] ), 'delete', absint( $item['ID'] ), $delete_nonce )
		];

		return $title . $this->row_actions( $actions );
	}
	/**
	* Render a column when no column specific method exists.
	*
	* @param array $item
	* @param string $column_name
	*
	* @return mixed
	*/
	public function column_default( $item, $column_name ) {
		switch ( $column_name ) {
			case 'address':
			case 'city':
			case 'phone':
			case 'ishome':
			case 'email':
				return $item[ $column_name ];
			default:
			return print_r( $item, true ); //Show the whole array for troubleshooting purposes
		}
	}
	/**
	* Render the bulk edit checkbox
	*
	* @param array $item
	*
	* @return string
	*/
	function column_cb( $item ) {
		return sprintf(
		'<input type="checkbox" name="bulk-delete[]" value="%s" />', $item['ID']
		);
	}
	/**
	* Associative array of columns
	*
	* @return array
	*/
	function get_columns() {
		$columns = [
		'cb' => '<input type="checkbox" />',
		'name' => __( 'Name', 'hd-locations' ),
		'ishome' => __( 'Show homepage', 'hd-locations' ),
		'address' => __( 'Address', 'hd-locations' ),
		'city' => __( 'City', 'hd-locations' ),
		'phone' => __( 'Phone', 'hd-locations' ),
		'email' => __( 'Email', 'hd-locations' )
		];

		return $columns;
	}
	/**
	* Columns to make sortable.
	*
	* @return array
	*/
	public function get_sortable_columns() {
		$sortable_columns = array(
		'name' => array( 'name', true ),
		'city' => array( 'city', false )
		);

		return $sortable_columns;
	}
	/**
	* Returns an associative array containing the bulk action
	*
	* @return array
	*/
	public function get_bulk_actions() {
		$actions = [
		'bulk-delete' => 'Delete'
		];

		return $actions;
	}
	/**
	* Handles data query and filter, sorting, and pagination.
	*/
	public function prepare_items() {

		$this->_column_headers = $this->get_column_info();

		/** Process bulk action */
		$this->process_bulk_action();

		$per_page = $this->get_items_per_page( 'locations_per_page', 5 );
		$current_page = $this->get_pagenum();
		$total_items = self::record_count();

		$this->set_pagination_args( [
		'total_items' => $total_items, //WE have to calculate the total number of items
		'per_page' => $per_page //WE have to determine how many items to show on a page
		] );

		$this->items = self::get_locations( $per_page, $current_page );
	}
	public function process_bulk_action() {
		//Detect when a bulk action is being triggered...
		if ( 'edit' === $this->current_action() ) {
			$id = isset($_GET['location']) && $_GET['location']?$_GET['location']:0;
			global $wpdb;
			$sql = "SELECT * FROM {$wpdb->prefix}locationsvn where ID = $id";
			$row = $wpdb->get_row( $sql);
			if(!$row)
			{
				header('location: ?page=HD_Location_VN');
				exit;
			}
			if(count($_POST)>0){
				$map = stripslashes($_POST['location_map']);
				preg_match('/src=\"(.+)\"\ width\=\"/',$map,$mats);
				if(isset($mats[1]))
					$_POST['location_map'] = $mats[1];				
				$table_name = $wpdb->prefix.'locations';
				$data_array = array(
					'name' => $_POST['location_name'],
					'city' => $_POST['location_city'],
					'ishome'=>$_POST['location_ishome'],
					'address' => $_POST['location_address'],
					'email' => $_POST['location_email'],
					'phone' => $_POST['location_phone'],
					'opening' => $_POST['location_opening'],
					'map' => $_POST['location_map']
				);
				$data_where = array('ID' =>$row->ID);
				$updated = $wpdb->update($table_name,$data_array,$data_where);
				if($updated)
				{
					$row->name = $_POST['location_name'];
					$row->city = $_POST['location_city'];
					$row->ishome = $_POST['location_ishome'];
					$row->address = $_POST['location_address'];
					$row->email = $_POST['location_email'];
					$row->phone = $_POST['location_phone'];
					$row->opening = $_POST['location_opening'];
					$row->map = $_POST['location_map'];
				}
			}
			//dd($row);
			?>
				<form action="" method="post" class="validate">
					<div class="form-field form-required term-name-wrap">
						<input name="location_id" id="location_id" type="hidden" value="<?=$row->ID?>" aria-required="true">
						<label for="location_name">Name</label><br>
						<input name="location_name" id="location_name" type="text" value="<?=$row->name?>" size="40" aria-required="true">
					</div>
					<div class="form-field form-required term-name-wrap">
						<label for="location_ishome">Show home page</label><br>
						<label ><input name="location_ishome" id="location_ishomeyes" type="radio" value="1" <?=$row->ishome==1?'checked':''?>> Yes</label>
						<label ><input name="location_ishome" id="location_ishomeno" type="radio" value="0" <?=$row->ishome!=1?'checked':''?>> No</label>
					</div>
					<div class="form-field form-required term-name-wrap">
						<label for="location_city">City</label><br>
						<input name="location_city" id="location_city" type="text" value="<?=$row->city?>" size="40" aria-required="true">
					</div>
					<div class="form-field form-required term-name-wrap">
						<label for="location_address">Address</label><br>
						<input name="location_address" id="location_address" type="text" value="<?=$row->address?>" size="255" aria-required="true">
					</div>
					<div class="form-field form-required term-name-wrap">
						<label for="location_email">Email</label><br>
						<input name="location_email" id="location_email" type="email" value="<?=$row->email?>" aria-required="true">
					</div>
					<div class="form-field form-required term-name-wrap">
						<label for="location_phone">Phone</label><br>
						<input name="location_phone" id="location_phone" type="text" value="<?=$row->phone?>" aria-required="true">
					</div>
					<div class="form-field form-required term-name-wrap">
						<label for="location_opening">Opening</label><br>
						<textarea name="location_opening" id="location_opening" aria-required="true"><?=$row->opening?></textarea>
					</div>
					<div class="form-field form-required term-name-wrap">
						<label for="location_map">GG Map</label><br>
						<textarea name="location_map" id="location_map" aria-required="true"><?=stripslashes($row->map)?></textarea>
					</div>
					<div class="form-field form-required term-name-wrap">
						<input type="submit" value="Save"/> <a href="?page=HD_Location_VN">Cancel</a>
					</div>
				</form>
			<?php
			exit;
		}
		//Detect when a bulk action is being triggered...
		if ( 'delete' === $this->current_action() ) {

			// In our file that handles the request, verify the nonce.
			$nonce = esc_attr( $_REQUEST['_wpnonce'] );

			if ( ! wp_verify_nonce( $nonce, 'sp_delete_location' ) ) {
				die( 'Go get a life script kiddies' );
			}
			else {
				self::delete_location( absint( $_GET['location'] ) );

				wp_redirect( esc_url( add_query_arg() ) );
				exit;
			}

		}

		// If the delete bulk action is triggered
		if ( ( isset( $_POST['action'] ) && $_POST['action'] == 'bulk-delete' )
		|| ( isset( $_POST['action2'] ) && $_POST['action2'] == 'bulk-delete' )
		) {

			$delete_ids = esc_sql( $_POST['bulk-delete'] );

			// loop over the array of record IDs and delete them
			foreach ( $delete_ids as $id ) {
				self::delete_location( $id );

			}

			wp_redirect( esc_url( add_query_arg() ) );
			exit;
		}
	}
}