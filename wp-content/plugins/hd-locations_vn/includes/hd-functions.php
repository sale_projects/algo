<?php
// The shortcode function homepage
	function hd_shortcode_home_location_vn() {
		global $wpdb;
		$sql = "SELECT * FROM {$wpdb->prefix}locationsvn where ishome=1 order by pos limit 4";
		$result = $wpdb->get_results( $sql);
		if($result)
		{
			?>
			<div id="col-449101248" class="col small-12 large-12">
				<div class="col-inner text-center box-shadow-3">
					<div class="row row-collapse align-middle align-center row-solid contact001" id="row-2025517292">
						<div id="col-411509139" class="col medium-12 small-12 large-6">
							<div class="col-inner text-center">
								<div class="row row-collapse row-full-width align-middle align-center row-solid" id="row-489564964">
									<div id="col-1592742701" class="col medium-6 small-6 large-6">
										<div class="col-inner text-center">
											<?php if(isset($result[0]) && $result[0]){ ?>
												<div class="item-location-home pointer" data-id="<?=$result[0]->ID?>" id="item-location-home-<?=$result[0]->ID?>">
													<div class="icon-box featured-box icon-box-center text-center is-xsmall">
														<div class="icon-box-img" style="width: 31px">
															<div class="icon">
																<div class="icon-inner">
																	<img width="240" height="240" src="/wp-content/uploads/2021/06/z2566318871698_67e0af48116bf1898e9bdd535a9dc9e6.jpg">
																</div>
															</div>
														</div>
														<div class="icon-box-text last-reset">
															<div id="text-3021208625" class="text">
																<h3><?=$result[0]->name?></h3>
																<style>
                                                                    #text-3021208625 {
                                                                        text-align: center;
                                                                    }
																</style>
															</div>
														</div>
													</div>
													<div id="text-4013885710" class="text">
														<p style="text-align: center;"><?=$result[0]->address?>
														</p>
														<style>
                                                            #text-4013885710 {
                                                                font-size: 0.75rem;
                                                                text-align: center;
                                                            }
                                                            @media (min-width: 550px) {
                                                                #text-4013885710 {
                                                                    font-size: 0.75rem;
                                                                }
                                                            }
														</style>
													</div>
												</div>
											<?php } ?>
											<?php if(isset($result[1]) && $result[1]){ ?>
												<div class="item-location-home pointer" data-id="<?=$result[1]->ID?>" id="item-location-home-<?=$result[1]->ID?>">
													<div class="icon-box featured-box icon-box-center text-center is-xsmall">
														<div class="icon-box-img" style="width: 31px">
															<div class="icon">
																<div class="icon-inner">
																	<img width="240" height="240" src="/wp-content/uploads/2021/06/z2566318871698_67e0af48116bf1898e9bdd535a9dc9e6.jpg">
																</div>
															</div>
														</div>
														<div class="icon-box-text last-reset">
															<div id="text-1080501638" class="text">
																<h3><?=$result[1]->name?></h3>
																<style>
                                                                    #text-1080501638 {
                                                                        text-align: center;
                                                                    }
																</style>
															</div>
														</div>
													</div>
													<div id="text-1064773939" class="text">
														<p><?=$result[1]->address?></p>
														<style>
                                                            #text-1064773939 {
                                                                font-size: 0.75rem;
                                                                text-align: center;
                                                            }
                                                            @media (min-width: 550px) {
                                                                #text-1064773939 {
                                                                    font-size: 0.75rem;
                                                                }
                                                            }
														</style>
													</div>
												</div>
											<?php } ?>
										</div>
										<style>
                                            #col-1592742701 > .col-inner {
                                                padding: 20px 0px 0px 0px;
                                            }
                                            @media (min-width: 550px) {
                                                #col-1592742701 > .col-inner {
                                                    padding: 0px 20px 0px 20px;
                                                }
                                            }
                                            @media (min-width: 850px) {
                                                #col-1592742701 > .col-inner {
                                                    padding: 0px 20px 0px 20px;
                                                }
                                            }
										</style>
									</div>
									<div id="col-565274154" class="col medium-6 small-6 large-6">
										<div class="col-inner text-center">
											<?php if(isset($result[2]) && $result[2]){ ?>
												<div class="item-location-home pointer" data-id="<?=$result[2]->ID?>" id="item-location-home-<?=$result[2]->ID?>">
													<div class="icon-box featured-box icon-box-center text-center is-xsmall">
														<div class="icon-box-img" style="width: 31px">
															<div class="icon">
																<div class="icon-inner">
																	<img width="240" height="240" src="/wp-content/uploads/2021/06/z2566318871698_67e0af48116bf1898e9bdd535a9dc9e6.jpg">
																</div>
															</div>
														</div>
														<div class="icon-box-text last-reset">
															<div id="text-3897349423" class="text">
																<h3><?=$result[2]->name?></h3>
																<style>
                                                                    #text-3897349423 {
                                                                        text-align: center;
                                                                    }
																</style>
															</div>
														</div>
													</div>
													<div id="text-3995151130" class="text">
														<p><?=$result[2]->address?></p>
														<style>
                                                            #text-3995151130 {
                                                                font-size: 0.75rem;
                                                                text-align: center;
                                                            }
                                                            @media (min-width: 550px) {
                                                                #text-3995151130 {
                                                                    font-size: 0.75rem;
                                                                }
                                                            }
														</style>
													</div>
												</div>
											<?php } ?>
											<?php if(isset($result[3]) && $result[3]){ ?>
												<div class="item-location-home pointer" data-id="<?=$result[3]->ID?>" id="item-location-home-<?=$result[3]->ID?>">
													<div class="icon-box featured-box icon-box-center text-center is-xsmall">
														<div class="icon-box-img" style="width: 31px">
															<div class="icon">
																<div class="icon-inner">
																	<img width="240" height="240" src="/wp-content/uploads/2021/06/z2566318871698_67e0af48116bf1898e9bdd535a9dc9e6.jpg">
																</div>
															</div>
														</div>
														<div class="icon-box-text last-reset">
															<div id="text-3993380425" class="text">
																<h3><?=$result[3]->name?></h3>
																<style>
                                                                    #text-3993380425 {
                                                                        text-align: center;
                                                                    }
																</style>
															</div>
														</div>
													</div>
													<div id="text-641561924" class="text">
														<p><?=$result[3]->address?></p>
														<style>
                                                            #text-641561924 {
                                                                font-size: 0.75rem;
                                                                text-align: center;
                                                            }
                                                            @media (min-width: 550px) {
                                                                #text-641561924 {
                                                                    font-size: 0.75rem;
                                                                }
                                                            }
														</style>
													</div>
												</div>
											<?php } ?>
										</div>
										<style>
                                            #col-565274154 > .col-inner {
                                                padding: 20px 0px 0px 0px;
                                            }
                                            @media (min-width: 550px) {
                                                #col-565274154 > .col-inner {
                                                    padding: 0 px0 20px 0px 20px;
                                                }
                                            }
                                            @media (min-width: 850px) {
                                                #col-565274154 > .col-inner {
                                                    padding: 0px 20px 0px 20px;
                                                }
                                            }
										</style>
									</div>
								</div>
							</div>
						</div>
						<div id="col-1124882240" class="col medium-12 small-12 large-6">
							<div class="col-inner text-center">
								<div id="text-3485638313" class="text">
									<?php
										foreach($result as $i=>$rs)
										{
											?>
											<div class="home-map-item <?=$i==0?'':'d-none'?>" id="home-map-<?=$rs->ID?>">
												<iframe src="<?=$rs->map?>"
												        width="100%" height="300" style="border:0;" allowfullscreen="" loading="lazy"></iframe>
											</div>
										<?php } ?>
									<style>
                                        #text-3485638313 {
                                            font-size: 0.75rem;
                                            line-height: 0.75;
                                            text-align: center;
                                        }
									</style>
								</div>
							</div>
							<style>

                                #col-1124882240 > .col-inner {
                                    border-radius: 62px;
                                }
							</style>
						</div>
					</div>
				</div>
			</div>
			<script>
                jQuery(function($){
                    $('.item-location-home').on('click',function(){
                        var _that = $(this),_id = _that.data('id');
                        $('.home-map-item').addClass('d-none');
                        $('#home-map-'+_id).removeClass('d-none');
                    })
                })
			</script>
			<?php
		}
	}
// Register shortcode
	add_shortcode('shortcode_location_home_vn', 'hd_shortcode_home_location_vn');
// The shortcode function homepage
	function hd_shortcode_contact_location_vn() {
		global $wpdb;
		$sql = "SELECT * FROM {$wpdb->prefix}locationsvn where city like '%ho chi minh%' order by pos";
		$result = $wpdb->get_results( $sql);
		if($result)
		{			
			$sql = "SELECT distinct city FROM {$wpdb->prefix}locationsvn order by pos";
			$cities = $wpdb->get_results( $sql);
			//if(isset($_GET['a']))
			//{
				//dd($cities[0]->city);
			//}
			?>
			<div class="row row-large row-full-width">
				<div style="padding: 0px !important;" class="col medium-12 small-12 large-12">
					<div class="col-inner" style="text-align:center;padding: 0px !important;">
						<span style="display:inline-block;    vertical-align: super;">Chọn Thành Phố:</span> <select id="citylocation" style="width:200px;display:inline-block">
							<?php foreach($cities as $city)
							{ ?>
								<option value="<?=$city->city?>"><?=$city->city?></option>
							<?php } ?>
						</select>
					</div>
				</div>
			</div>
			<div class="row row-large row-full-width" id="row-1995872802">
				<div id="col-1291213819" style="padding: 0px !important;margin: 0px !important;" class="col medium-12 small-12 large-12">
					<div class="col-inner" style="padding: 0px !important;" id="loadlocationdiv">
						<div class="tabbed-content contactuspage">
							<ul class="nav nav-outline nav-normal nav-size-xlarge nav-center">
								<?php foreach($result as $i=>$rs){ ?>
									<li class="tab has-icon <?=$i==0?'active':''?>"><a href="#location_tab-<?=$rs->ID?>"><span><?=$rs->name?></span></a></li>
								<?php } ?>
							</ul>
							<?php foreach($result as $i=>$rs){
								$_id = time().$i;
								?>
								<div class="tab-panels">
									<div class="panel entry-content <?=$i==0?'active':''?>" id="location_tab-<?=$rs->ID?>">
										<div class="row row-full-width">
											<div class="col medium-6 small-12 large-6">
												<div class="row">
													<div style="padding: 0px !important;" class="col medium-6 small-12 large-6">
														<div class="col-inner text-center">
															<div class="img has-hover x md-x lg-x y md-y lg-y" id="image_<?=$_id?>">
																<div class="img-inner dark">
																	<img width="240" height="240" style="width: 25px;height: 25px" src="/wp-content/uploads/2021/06/z2566318871698_67e0af48116bf1898e9bdd535a9dc9e6.jpg" >
																</div>
																<style>
                                                                    #image_<?=$_id?> {
                                                                        width: 15%;
                                                                    }
                                                                    @media (min-width:550px) {
                                                                        #image_<?=$_id?> {
                                                                            width: 10%;
                                                                        }
                                                                    }
																</style>
															</div>
															<h3 class="uppercase">Địa Chỉ</h3>
															<p><?=str_replace("\n",'<br>',$rs->address)?>
															</p>
														</div>
													</div>
													<div class="col medium-5 small-12 large-6">
														<div class="col-inner text-center">
															<div class="img has-hover x md-x lg-x y md-y lg-y" id="image_<?=$_id.'2'?>">
																<div class="img-inner dark">
																	<img width="240" height="240" style="width: 25px;height: 25px" src="/wp-content/uploads/2021/06/z2566318881891_e3ac3a69174d674070b94bead152e9a5.jpg">
																</div>
																<style>
                                                                    #image_<?=$_id.'2'?> {
                                                                        width: 15%;
                                                                    }
                                                                    @media (min-width:550px) {
                                                                        #image_<?=$_id.'2'?> {
                                                                            width: 10%;
                                                                        }
                                                                    }
																</style>
															</div>
															<h3 class="uppercase">Thời gian mở cửa</h3>
															<p><?=str_replace("\n",'<br>',$rs->opening)?>
															</p>
														</div>
													</div>
												</div>
												<div class="row">
													<div  style="padding: 0px !important;" class="col medium-6 small-12 large-6">
														<div class="col-inner text-center">
															<div class="img has-hover x md-x lg-x y md-y lg-y" id="image_<?=$_id.'3'?>">
																<div class="img-inner dark">
																	<img width="240" height="240" style="width: 25px;height: 27px" src="/wp-content/uploads/2021/06/z2566318878868_7158e3a13343836f207a4eb0b731befc.jpg" >
																</div>
																<style>
                                                                    #image_<?=$_id.'3'?> {
                                                                        width: 15%;
                                                                    }
                                                                    @media (min-width:550px) {
                                                                        #image_<?=$_id.'3'?> {
                                                                            width: 10%;
                                                                        }
                                                                    }
																</style>
															</div>
															<h3 class="uppercase">E-mail</h3>
															<p><?=$rs->email?></p>
														</div>
													</div>
													<div style="padding: 0px !important;" class="col medium-6 small-12 large-6">
														<div class="col-inner text-center">
															<div class="img has-hover x md-x lg-x y md-y lg-y" id="image_<?=$_id.'4'?>">
																<div class="img-inner dark">
																	<img width="240" height="240" style="width: 25px;height: 25px" src="/wp-content/uploads/2021/06/z2566318878895_0537158bdd9c8f3d746a4d89296ece9f.jpg">
																</div>
																<style>
                                                                    #image_<?=$_id.'4'?> {
                                                                        width: 15%;
                                                                    }
                                                                    @media (min-width:550px) {
                                                                        #image_<?=$_id.'4'?> {
                                                                            width: 10%;
                                                                        }
                                                                    }
																</style>
															</div>
															<h3 class="uppercase">Điện Thoại</h3>
															<p><?=$rs->phone?></p>
														</div>
													</div>
												</div>
											</div>
											<div style="padding: 0px !important;" class="col medium-6 small-12 large-6">
												<div class="col-inner">
													<div class="text">
														<iframe src="<?=$rs->map?>" width="100%" height="400" style="border:0;" allowfullscreen="" loading="lazy"></iframe>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							<?php } ?>
						</div>
					</div>
					<style>
                        .tab-panels{
                            padding:0;
                        }
                        .contactuspage > ul
                        {
                            margin-bottom:25px;
                        }
					</style>
				</div>
			</div>
			
			<?php
		}
	}
// Register shortcode
	add_shortcode('shortcode_location_contact_vn', 'hd_shortcode_contact_location_vn');
//load locations ajax
	add_action('wp_ajax_loadlocation', 'load_location_vn');
	add_action('wp_ajax_nopriv_loadlocation', 'load_location_vn');
	function load_location_vn() {//sleep(50);
		if(!isset($_POST['city']) || !$_POST['city'])
		{
			echo '<div style="text-align:center;color:red">Data not found!</div>';
			exit;
		}
		global $wpdb;
		$sql = "SELECT * FROM {$wpdb->prefix}locationsvn where city like %s order by pos";
		$result = $wpdb->get_results( $wpdb->prepare($sql,'%'.$_POST['city'].'%'));
		if($result)
		{
			?>
			<div class="tabbed-content contactuspage">
				<ul class="nav nav-outline nav-normal nav-size-xlarge nav-center">
					<?php foreach($result as $i=>$rs){ ?>
						<li class="tab has-icon <?=$i?> <?=$i==0?'active':''?>"><a href="#location_tab-<?=$rs->ID?>"><span><?=$rs->name?></span></a></li>
					<?php } ?>
				</ul>
				<?php foreach($result as $i=>$rs){
					$_id = time().$i;
					?>
					<div class="tab-panels">
						<div class="panel entry-content <?=$i==0?'active':''?>" id="location_tab-<?=$rs->ID?>">
							<div class="row row-full-width">
								<div class="col medium-6 small-12 large-6">
									<div class="row">
										<div style="padding: 0px !important;" class="col medium-6 small-12 large-6">
											<div class="col-inner text-center">
												<div class="img has-hover x md-x lg-x y md-y lg-y" id="image_<?=$_id?>">
													<div class="img-inner dark">
														<img width="240" height="240" src="/wp-content/uploads/2021/05/map-pin-2-fill-2.png" >
													</div>
													<style>
                                                        #image_<?=$_id?> {
                                                            width: 15%;
                                                        }
                                                        @media (min-width:550px) {
                                                            #image_<?=$_id?> {
                                                                width: 10%;
                                                            }
                                                        }
													</style>
												</div>
												<h3 class="uppercase">Address</h3>
												<p><?=str_replace("\n",'<br>',$rs->address)?>
												</p>
											</div>
										</div>
										<div style="padding: 0px !important;" class="col medium-5 small-12 large-6">
											<div class="col-inner text-center">
												<div class="img has-hover x md-x lg-x y md-y lg-y" id="image_<?=$_id.'2'?>">
													<div class="img-inner dark">
														<img width="240" height="240" src="/wp-content/uploads/2021/05/time-fill.png">
													</div>
													<style>
                                                        #image_<?=$_id.'2'?> {
                                                            width: 15%;
                                                        }
                                                        @media (min-width:550px) {
                                                            #image_<?=$_id.'2'?> {
                                                                width: 10%;
                                                            }
                                                        }
													</style>
												</div>
												<h3 class="uppercase">Thời gian mở cửa</h3>
												<p><?=str_replace("\n",'<br>',$rs->opening)?>
												</p>
											</div>
										</div>
									</div>
									<div class="row">
										<div style="padding: 0px !important;" class="col medium-6 small-12 large-6">
											<div class="col-inner text-center">
												<div class="img has-hover x md-x lg-x y md-y lg-y" id="image_<?=$_id.'3'?>">
													<div class="img-inner dark">
														<img width="240" height="240" src="/wp-content/uploads/2021/05/mail-open-fill.png" >
													</div>
													<style>
                                                        #image_<?=$_id.'3'?> {
                                                            width: 15%;
                                                        }
                                                        @media (min-width:550px) {
                                                            #image_<?=$_id.'3'?> {
                                                                width: 10%;
                                                            }
                                                        }
													</style>
												</div>
												<h3 class="uppercase">E-mail</h3>
												<p><?=$rs->email?></p>
											</div>
										</div>
										<div style="padding: 0px !important;" class="col medium-6 small-12 large-6">
											<div class="col-inner text-center">
												<div class="img has-hover x md-x lg-x y md-y lg-y" id="image_<?=$_id.'4'?>">
													<div class="img-inner dark">
														<img width="240" height="240" src="/wp-content/uploads/2021/05/phone-fill-1.png">
													</div>
													<style>
                                                        #image_<?=$_id.'4'?> {
                                                            width: 15%;
                                                        }
                                                        @media (min-width:550px) {
                                                            #image_<?=$_id.'4'?> {
                                                                width: 10%;
                                                            }
                                                        }
													</style>
												</div>
												<h3 class="uppercase">Điện Thoại</h3>
												<p><?=$rs->phone?></p>
											</div>
										</div>
									</div>
								</div>
								<div class="col medium-6 small-12 large-6">
									<div class="col-inner">
										<div class="text">
											<iframe src="<?=$rs->map?>" width="100%" height="400" style="border:0;" allowfullscreen="" loading="lazy"></iframe>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
                    <style>
                        .col-inner,.col.medium-12.small-12.large-12,.col.medium-6.small-12.large-6{
                            padding: 0px !important;
                        }{
                            padding: 0px !important;
                        }
                        .tab-panels .col.medium-6.small-12.large-6,.col.medium-6.small-12.large-6{
                            padding: 0px !important;
                        }
                        #row-2135845253 > .col > .col-inner{
                            padding: 0px !important;
                        }
                    </style>
				<?php } ?>
			</div>
			<script> window.location.hash = '';Flatsome.behaviors.tabs.attach(document)</script>
			<?php
			exit;
		}else{
			echo '<div style="text-align:center;color:red">Data not found!</div>';
			exit;
		}
	}