<?php 
class HD_Location {

	// class instance
	static $instance;

	// customer WP_List_Table object
	public $lcoations_obj;

	// class constructor
	public function __construct() {
		add_filter( 'set-screen-option', [ __CLASS__, 'set_screen' ], 10, 3 );
		add_action( 'admin_menu', [ $this, 'plugin_menu' ] );
	}


	public static function set_screen( $status, $option, $value ) {
		return $value;
	}

	public function plugin_menu() {

		$hook = add_menu_page(
			'HD Location',
			'HD Location',
			'manage_options',
			'HD_Location',
			[ $this, 'plugin_settings_page' ]
		);
		add_submenu_page('HD_Location', 'Add new', 'Add new', 'manage_options','hd_addnew_location',[ $this,'hd_addnew_location']);
		add_action( "load-$hook", [ $this, 'screen_option' ] );

	}
	public function hd_addnew_location() {
			global $wpdb;
			if(count($_POST)>0){
				$table_name = $wpdb->prefix.'locations';
				$map = stripslashes($_POST['location_map']);
				preg_match('/src=\"(.+)\"\ width\=\"/',$map,$mats);
				if(isset($mats[1]))
					$_POST['location_map'] = $mats[1];	
				$data_array = array(
					'name' => $_POST['location_name'],
					'city' => $_POST['location_city'],
					'ishome'=>$_POST['location_ishome'],
					'address' => $_POST['location_address'],
					'email' => $_POST['location_email'],
					'phone' => $_POST['location_phone'],
					'opening' => $_POST['location_opening'],
					'map' => $_POST['location_map']
				);
				$data_where = array('ID' =>$row->ID);
				$inserted = $wpdb->insert($table_name,$data_array);
				if($inserted)
				{
					$msg = 'Add new location success';
				}else{
					$msg = 'Add new location failed';
				}
			}
		?>
		<div class="wrap">
			<h2>Add new location</h2>
			<?=$msg??''?>
			<form action="" method="post" class="validate">
					<div class="form-field form-required term-name-wrap">
						<label for="location_name">Name</label><br>
						<input name="location_name" id="location_name" type="text" value="" size="40" aria-required="true">
					</div>
					<div class="form-field form-required term-name-wrap">
						<label for="location_ishome">Show home page</label><br>
						<label ><input name="location_ishome" id="location_ishomeyes" type="radio" value="1" > Yes</label>
						<label ><input name="location_ishome" id="location_ishomeno" type="radio" value="0" checked> No</label>
					</div>
					<div class="form-field form-required term-name-wrap">
						<label for="location_city">City</label><br>
						<input name="location_city" id="location_city" type="text" value="" size="40" aria-required="true">
					</div>
					<div class="form-field form-required term-name-wrap">
						<label for="location_address">Address</label><br>
						<input name="location_address" id="location_address" type="text" value="" size="255" aria-required="true">
					</div>
					<div class="form-field form-required term-name-wrap">
						<label for="location_email">Email</label><br>
						<input name="location_email" id="location_email" type="email" value="" aria-required="true">
					</div>
					<div class="form-field form-required term-name-wrap">
						<label for="location_phone">Phone</label><br>
						<input name="location_phone" id="location_phone" type="text" value="" aria-required="true">
					</div>
					<div class="form-field form-required term-name-wrap">
						<label for="location_opening">Opening</label><br>
						<textarea name="location_opening" id="location_opening" aria-required="true"></textarea>
					</div>
					<div class="form-field form-required term-name-wrap">
						<label for="location_map">GG Map</label><br>
						<textarea name="location_map" id="location_map" aria-required="true"></textarea>
					</div>
					<div class="form-field form-required term-name-wrap">
						<input type="submit" value="Save"/> <a href="?page=HD_Location">Cancel</a>
					</div>
				</form>
		</div>
	<?php
	}
	/**
	 * Plugin settings page
	 */
	public function plugin_settings_page() {
		?>
		<div class="wrap">
			<h2>HD Locations <a href="?page=hd_addnew_location" class="page-title-action">Add New</a></h2>

			<div id="poststuff">
				<div id="post-body" class="metabox-holder">
					<div id="post-body-content">
						<div class="meta-box-sortables ui-sortable">
							<form method="post">
								<?php
								$this->lcoations_obj->prepare_items();
								$this->lcoations_obj->display(); ?>
							</form>
						</div>
					</div>
				</div>
				<br class="clear">
			</div>
		</div>
	<?php
	}

	/**
	 * Screen options
	 */
	public function screen_option() {

		$option = 'per_page';
		$args   = [
			'label'   => 'Locations',
			'default' => 5,
			'option'  => 'locations_per_page'
		];

		add_screen_option( $option, $args );

		$this->lcoations_obj = new Location_Table();
	}


	/** Singleton instance */
	public static function get_instance() {
		if ( ! isset( self::$instance ) ) {
			self::$instance = new self();
		}

		return self::$instance;
	}

}
add_action( 'plugins_loaded', function () {
	HD_Location::get_instance();
} );